#!/bin/bash -e
export PYTHONPATH="/schedule:/code:/code/lintian-brush:/code/silver-platter:/code/breezy:/code/dulwich:/code/debmutate:/code/upstream-ontologist"
python3 -m janitor.policy --config=/etc/janitor/janitor.conf --policy=/etc/janitor/policy.conf --gcp-logging
/schedule/upstream-codebases.py --extra-upstream-projects=/etc/janitor/extra_upstream_projects.conf | python3 -m janitor.package_metadata --package-overrides=/etc/janitor/package_overrides.conf --distribution=upstream --config=/etc/janitor/janitor.conf --gcp-logging "$@"
/schedule/udd-package-metadata.py --gcp-logging | python3 -m janitor.package_metadata --distribution=unstable --package-overrides=/etc/janitor/package_overrides.conf --config=/etc/janitor/janitor.conf --gcp-logging "$@"
(
   python3 /schedule/unchanged-candidates.py
   python3 /schedule/upstream-unchanged-candidates.py --config=/etc/janitor/janitor.conf
   python3 /schedule/scrub-obsolete-candidates.py
   python3 /schedule/cme-candidates.py
   python3 /schedule/lintian-fixes-candidates.py
   python3 /schedule/fresh-releases-candidates.py
   python3 /schedule/fresh-snapshots-candidates.py
   python3 /schedule/multi-arch-candidates.py
   python3 /schedule/orphan-candidates.py
   python3 /schedule/mia-candidates.py
   python3 /schedule/uncommitted-candidates.py
   python3 /schedule/debianize-candidates.py --config=/etc/janitor/janitor.conf
) | python3 -m janitor.candidates --config=/etc/janitor/janitor.conf --gcp-logging "$@"
python3 -m janitor.schedule --config=/etc/janitor/janitor.conf --gcp-logging "$@"
