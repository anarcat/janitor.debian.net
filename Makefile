all: docker-schedule k8s

docker-schedule:
	docker build -f Dockerfile_schedule . -t eu.gcr.io/debian-janitor/schedule
	docker push eu.gcr.io/debian-janitor/schedule

k8s:
	$(MAKE) -C k8s

.PHONY: k8s
