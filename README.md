This repository contains the configuration for https://janitor.debian.net/.
It's an instance of the Janitor project, which can be found at
https://salsa.debian.org/jelmer/debian-janitor.

schedule/ contains the various scripts for importing package metadata
and candidates.

See the k8s/ directory for the kubernetes configuration.
